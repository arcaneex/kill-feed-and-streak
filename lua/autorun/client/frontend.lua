include( 'autorun/config.lua' )

surface.CreateFont( "Killfeed Average", {

	font = "BebasNeue",
	size = 30

	
} )


local fmt = string.format

local Feed = { }

local Streaks = { }

local function GenerateFeed( text )

	local SubFeed = { }
	
	SubFeed.Text = text
	
	SubFeed.Time = CurTime() + KillFeedCfg.Time
	
	table.insert( Feed, SubFeed )
	
end

local function GenerateStreak( text )

	local SubFeed = { }
	
	SubFeed.Text = text
	
	SubFeed.Time = CurTime() + KillFeedCfg.Time
	
	table.insert( Streaks, SubFeed )
	
end

local function DisplayFeed( ICol )
	
	for n, x in pairs( Feed ) do
	
		local Alpha = 255 * math.Clamp( x.Time-CurTime(), 0, 1 )
		
		local Col = KillFeedCfg.MainColor
		
		local Col = Color( Col.r, Col.g, Col.b, Alpha )
		
		local SubCol = Color( KillFeedCfg.OutlineColor.r, KillFeedCfg.OutlineColor.g, KillFeedCfg.OutlineColor.b, Alpha )
		
		if n == 1 then
		
			x.Num = 0
			
		else
		
			x.Num = n - 1
			
		end
		
		draw.SimpleTextOutlined( x.Text, "Killfeed Average", KillFeedCfg.PosX, KillFeedCfg.PosY + ( x.Num * 25 ), Col, TEXT_ALIGN_RIGHT, nil, 1, SubCol)
		
		local Alpha = 255
		
		if CurTime() > x.Time then table.remove( Feed, n ) end
		
	end
	
end

local function DisplayStreaks()

	for n, x in pairs( Streaks ) do
	
		local Alpha = 255 * math.Clamp( x.Time-CurTime(), 0, 1 )
		
		local Col = KillFeedCfg.StreakColor
		
		local Col = Color( Col.r, Col.g, Col.b, Alpha )
		
		local SubCol = Color( KillFeedCfg.OutlineColor.r, KillFeedCfg.OutlineColor.g, KillFeedCfg.OutlineColor.b, Alpha )
		
		if n == 1 then
		
			x.Num = 0
			
		else
		
			x.Num = n - 1
			
		end
		
		draw.SimpleTextOutlined( x.Text, "Killfeed Average", KillFeedCfg.StreakX, KillFeedCfg.StreakY + ( x.Num * 25 ), Col, TEXT_ALIGN_CENTER, nil, 1, SubCol)
		
		local Alpha = 255
		
		if CurTime() > x.Time then table.remove( Streaks, n ) end
		
	end
		
end

local function Streaks( streaker )
	
	local StreakKills = streaker:GetNWInt( "StreakKills", 0 ) + 1
	
	local Streak
	
	if StreakKills == 3 then
		
		Streak = fmt( KillFeedCfg.Streak3, streaker:GetName() ) or fmt( "%s killed three people!" )
		
	end
	
	if StreakKills == 4 then
		
		Streak = fmt( KillFeedCfg.Streak4, streaker:GetName() ) or fmt( "%s killed four people!" )
		
	end
	
	if StreakKills == 5 then
		
		Streak = fmt( KillFeedCfg.Streak5, streaker:GetName() ) or fmt( "%s killed five people!" )
		
	end

	if StreakKills >= 6 then
		
		Streak = fmt( KillFeedCfg.Streak6, streaker:GetName() ) or fmt( "%s killed six people!" )
		
	end
	
	if Streak then
	
		GenerateStreak( Streak )
		
	end
	
end

local function Receiver()

	local victim = net.ReadEntity()
	
	local killer = net.ReadEntity()
	
	local ProperName
	
	if type( killer ) != 'Player' then
	
		KillerName = killer:GetClass()
		
	else
	
		KillerName = killer:GetName()
		
	end

	if type( victim ) != 'Player' then
	
		VictimName = victim:GetClass()
		
	else
	
		VictimName = victim:GetName()
		
	end
	
	local Text = fmt( "%s has killed %s", KillerName, VictimName )
	
	if KillFeedCfg.ShowCause and killer != victim and type( killer ) == 'Player' then
	
		local wep = killer:GetActiveWeapon():GetClass()

		Text = Text .. fmt( " using %s", wep )
		
	end
	
	if killer == victim then
	
		Text = fmt( "%s has killed himself!", victim:GetName() )
	
	end

	GenerateFeed( Text )
	
	if KillFeedCfg.Streaks and killer != victim then
	
		Streaks( killer )
		
	end
	
end

net.Receive( "sf_kill_feed", Receiver )
hook.Add( "HUDPaint", "Displaying streak feed", DisplayStreaks )
hook.Add( "HUDPaint", "Displaying kill feed", DisplayFeed )

hook.Add( "Initialize", "Hide og feed", function()

	function GAMEMODE:DrawDeathNotice(x, y)

		return
	
	end
	
end)