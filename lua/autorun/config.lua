KillFeedCfg = { }

if CLIENT then

	KillFeedCfg.PosX = ScrW() - 10 -- X position of the killfeed, ScrW() to use the screen resolution (x)

	KillFeedCfg.PosY = 0 -- Y position of the killfeed, ScrH() to use the screen resolution (y)

	KillFeedCfg.Time = 4 --time to stay

	KillFeedCfg.Streaks = true --show streaks and stuff? (double kill etc)
	
	KillFeedCfg.Streak3 = "★ %s is on a killing spree! ★" --killing 3 people, if enabled. use %s to use the name of the streak(er)
	
	KillFeedCfg.Streak4 = "★ %s is dominating! ★" --killing 4 people, if enabled. use %s to use the name of the streak(er)

	KillFeedCfg.Streak5 = "★ %s is on a rampage!! ★" --killing 5 people, if enabled. use %s to use the name of the streak(er)

	KillFeedCfg.Streak6 = "★ %s is godlike!!!!!!!!! ★" --killing 6 people, if enabled. use %s to use the name of the streak(er)

	KillFeedCfg.MainColor = Color(211, 84, 0) --text color

	KillFeedCfg.OutlineColor = Color( 20, 20, 20 ) --outline color
	
	KillFeedCfg.StreakColor = Color(255, 60, 60) --text color (streak)
	
	KillFeedCfg.StreakX = ScrW() / 2  -- X position of the killfeed, ScrW() to use the screen resolution (x), ScrW() / 2 to position it in the center

	KillFeedCfg.StreakY = 100 -- Y position of the killfeed, ScrH() to use the screen resolution (y)
	
	KillFeedCfg.ShowCause = true --show the cause?	
	
end

if SERVER then
	
	KillFeedCfg.StreakTime = 8 --in how much seconds reset the counter? (since last time)
	
end