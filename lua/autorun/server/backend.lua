AddCSLuaFile( 'autorun/client/frontend.lua' )

util.AddNetworkString( "sf_kill_feed" )

local function Sender( victim, inf, killer )
	
	net.Start( "sf_kill_feed" )
	
		net.WriteEntity( victim )
		net.WriteEntity( killer )
		
	net.Broadcast()
	
	local streak = killer:GetNWInt( "StreakKills", 0 )
	
	killer:SetNWInt( "StreakKills", streak + 1 )
	
	if !killer.TimerStarted then
		
		killer.TimerStarted = true
		
		timer.Create( killer:GetName() .. " streak controller", KillFeedCfg.StreakTime, 0, function() 
			
			killer:SetNWInt( "StreakKills", 0 ) 
			
		end )
		
	else
		
		timer.Stop( killer:GetName() .. " streak controller" )
		
		timer.Start( killer:GetName() .. " streak controller" )
		
	end
	
end

hook.Add( 'PlayerDeath', 'Sending death details to the feed', Sender )

hook.Add( 'PlayerSpawn', 'Spawning hook to set streak int', function( ply )

	ply:SetNWInt( "StreakKills", 0 )
	
end)

hook.Add( 'PlayerSpawn', 'Spawning hook to set streak int', function( ply )

	ply:SetNWInt( "StreakKills", 0 )
	
end)

hook.Add( 'OnNPCKilled', 'asdsdasd', function( victim, killer, inf )

	net.Start( "sf_kill_feed" )
	
		net.WriteEntity( victim )
		net.WriteEntity( killer )
		
	net.Broadcast()
	
	if type( killer ) != 'Player' then return end
	
	local streak = killer:GetNWInt( "StreakKills", 0 )
	
	killer:SetNWInt( "StreakKills", streak + 1 )
	
	if !killer.TimerStarted then
		
		killer.TimerStarted = true
		
		timer.Create( killer:GetName() .. " streak controller", KillFeedCfg.StreakTime, 0, function() 
			
			killer:SetNWInt( "StreakKills", 0 ) 
			
		end )
		
	else
		
		timer.Stop( killer:GetName() .. " streak controller" )
		
		timer.Start( killer:GetName() .. " streak controller" )
		
	end
	
end)